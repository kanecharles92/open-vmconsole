<#
.SYNOPSIS
    Open VM remote consoles via PowerCLI and VMRC.
.DESCRIPTION
    This cmdlet constructs URIs to feed to the VMRC client to open VM consoles programatically rather than using the web browser.
.EXAMPLE
    PS C:\> Get-VM vm1,vm2,vm3 | Open-VMConsole
    This would open 3 separate VMRC instances, a single one to each VM.
.INPUTS
    [VMware.VimAutomation.Types.VirtualMachine[]]$VM
        One or more VM objects
    [String]$VMRCPath
        Path to VMRC executable (defaults to default Windows location but should work on Linux and OS X too if path is specified)    
.OUTPUTS
    None
.NOTES
    Accepts VM objects from the pipeline and can be from more than 1 vCenter server.
#>
function Open-VMConsole {
    [CmdletBinding()]
    param (
        # One or more VMs
        [ValidateNotNullOrEmpty()]
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [VMware.VimAutomation.Types.VirtualMachine[]]
        $VM,

        # VMRC executable path
        [Parameter(Mandatory=$false)]
        [String]
        $VMRCPath = "C:\Program Files (x86)\VMware\VMware Remote Console\vmrc.exe"
    )
    
    begin {

    }
    
    process {
        # Get session manager handle
        $sessionManager = Get-View -Id Sessionmanager

        # Get distinct service url list of connected vCenter servers
        $distinctServiceURLs = ($VM | Select-Object @{N="ServiceUrl";E={$_.ExtensionData.Client.ServiceUrl}} -Unique)
        Write-Verbose "Distinct Service URLs: $($distinctServiceURLs.ServiceURL -join ", ")"

        # Get distinct VC server list
        $distinctVCs = @()
        
        # Strip the head/tail of unecessary characters from VC addresses
        foreach ($serviceURL in $distinctServiceURLs.ServiceUrl) {
            $vcWithoutHead = $serviceURL.SubString(8)
            $vcWithoutHeadOrTail = $vcWithoutHead.SubString(0,$vcWithoutHead.Length-4)
            $distinctVCs += $vcWithoutHeadOrTail
        }
        
        Write-Verbose "Distinct vCenter Addresses: $($distinctVCs -join ", ")"
        
        # For each distinct VC we are targeting
        foreach ($vc in $distinctVCs) {
            # Get a session handle
            $sessionHandle = Get-View -Id Sessionmanager | ? {$_.Client.ServiceUrl -like "*$vc*"}
        
            # Get list of all VMs in current VC
            $currentVCVMs = $VM | ? {$_.ExtensionData.Client.ServiceUrl -like "*$vc*"}

            # Get current VC Session Clone Ticket
            $currentVCTicket =  $sessionHandle.AcquireCloneTicket()

            # For each VM on current VC
            foreach ($targetVM in $currentVCVMs) {
                # Get VM Moref ID
                $vmid = $targetVM.ExtensionData.Moref.Value

                # Start VMRC for current VM
                try
                {
                    Start-Process -FilePath $VMRCPath -ArgumentList "vmrc://clone:$($currentVCTicket)@$($vc)/?moid=$($vmid)"
                }
                catch
                {
                    Write-Host $ErrorMessage
                }
            }
        }
    }
    
    end {
        
    }
}